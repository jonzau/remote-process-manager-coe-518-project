/**
*
* By: Ionathan Zauritz
* Final Project for COE 518 Fall 2013
* Client
*
**/

#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <netdb.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <arpa/inet.h>

#define maxStrSize 4096
#define port 5301

void connectToServer(void);
void error(const char *msg); //error function

char *server_ip;

int main(int argc, char *argv[])
{
	srand(time(0)); // Initialize random generator
	if(argc != 2)
	{
		printf("\n Usage: %s <ip of server> \n",argv[0]);
		return 1;
	}

    server_ip = argv[1]; //first argument is the server IP the client wants to connect to


	//Call the function to connect to server
    connectToServer();

	return 0;
}

void connectToServer(void)
{
	int sock_id = 0; // the variable for keeping socket identifier
	char command[maxStrSize];	// the variable for keeping command to send
	char recv_buff[maxStrSize]; //  the buffer that is used for keeping the data from the socket
    int n = 0, se;

	//Creates the socket that is going to be used for communicating with server
    sock_id = socket(PF_INET, SOCK_STREAM, 0);
    if(sock_id == -1){
        error("Socket was not created!\n");
    }
	//Declaration and initialization of the variable that keeps the socket address of server
    struct sockaddr_in clientIp;
    memset((char *) &clientIp, 0, sizeof(clientIp));
    clientIp.sin_family = AF_INET;
    inet_pton(AF_INET, server_ip, &clientIp.sin_addr.s_addr);
    clientIp.sin_port = htons((u_short) port);

	//Tries to establish a connection with server
    int co = connect(sock_id, (struct sockaddr *) &clientIp, sizeof(clientIp));
    if(co == -1){
        error("Error connecting!\n");
    }

    // receives from server
    n = recv(sock_id, recv_buff, sizeof(recv_buff), 0);
    if(n == -1){
        error("Error receiving from server!\n");
    }

    printf("%s", recv_buff);

    while(1){
        strcpy(command, "");
        while(command[0] == '\0'){
            gets(command);
        }
        //Sends command
        se = send(sock_id, command, sizeof(command), 0);
        if(se == -1){
            error("Error sending command!\n");
        }

        if(strcmp(command, "exit") == 0){
            break;
        }

        recv_buff[strlen(recv_buff)-1] = '0'; //make it not '>' so it enters the loop
        while (recv_buff[strlen(recv_buff)-1] != '>'){
            n = recv(sock_id, recv_buff, sizeof(recv_buff), 0);
            if(n == -1){
                error("Error receiving from server!\n");
            }

            printf("%s", recv_buff);
        }

    }

    //Closes the socket
	close(sock_id);
}

void error(const char *msg)
{
    perror(msg);
    exit(1);
}

