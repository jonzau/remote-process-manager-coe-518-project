/**
*
* By: Ionathan Zauritz
* Final Project for COE 518 Fall 2013
* Server
*
**/

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <time.h>
#include <sys/wait.h>

#define port 5301
#define maxStrSize 4096
#define maxConnections 1000

void connection(int conn_id, char clientIp[]);
void sys(char cmd[]);
void settime(char curTime[]);
void toLower(char str[], int n);
void error(const char *msg);
long get_process_mem(int prc_id);
void *watchFunction(void * threadNum);
void *interface(void);

typedef struct watchData{
    int time;
    int pid;
    int conn_id;
} watchData;

watchData w[8];
char watchLog[maxStrSize];
pthread_mutex_t mutex;// = PTHREAD_MUTEX_INITIALIZER;
int pids[maxConnections], pidsNum = 0; //keeps track of connection processes

main(int argc, char *argv[]){

	int listenfd = 0, conn_id = 0; // the listening socket identifier and current connection identifier
    //Creates the master socket
    int sock = socket(PF_INET, SOCK_STREAM, 0);
    if(sock == -1){
        error("Socket was not created!\n");
    }
    //the struct variable that keeps the socket address
    struct sockaddr_in ip, clientIp;
    socklen_t alen;

    //initialization of socket address variable
    memset((char *) &ip, 0, sizeof(ip));
    ip.sin_family = AF_INET;
    ip.sin_addr.s_addr = INADDR_ANY; //inet_addr("127.0.0.1");
    ip.sin_port = htons((u_short) port);

    //binds the socket to its address
    int bi = bind(sock, (struct sockaddr *) &ip, sizeof(ip));
    if(bi == -1){
        error("Error binding!\n");
    }

    //D: starts listening on that socket
    int li = listen(sock, maxConnections);
    if(li == -1){
        error("Error listening!\n");
    }

    pthread_t interfaceThread;
    int iret = pthread_create(&interfaceThread, NULL, interface, NULL);
                if(iret != 0){
                    error("interfaceThread not generated!\n");
                }

    while(1) // listens for incoming connections untill program exits manually
    {
		conn_id = 0;
		//E: Wait for a connection request and accept it
		//alen = sizeof(ip);
		conn_id = accept(sock, (struct sockaddr *) &ip, &alen);
		if(conn_id == -1){
            error("Error accepting!\n");
		}

		//if the connection has been established it creates a process that handles the connection
		pid_t pid;
        pid = fork();
        if(pid < 0){
            error("The fork was unsuccessful!\n");
        }

        getpeername(conn_id,(struct sockaddr *)&clientIp,&alen); //gets client's ip
        char *clientIpStr = inet_ntoa(clientIp.sin_addr);

		//exit the loop if child
		if(pid == 0){
			connection(conn_id, clientIpStr);
			break;
		}
		else{
			//(optional) something to do in the parent
			pids[pidsNum] = pid;
			pidsNum++;
			printf("Connection established with client ip %s in process %d.\n", clientIpStr, pid);

		}
     }

	 return 0;
}

void connection(int conn_id, char clientIp[])
{
	char command[maxStrSize]; // the buffer that is used for keeping the data read from the socket
	int n=0; // number of characters read from socket

	int se, re;
	char curTime[256];
	char toSend[maxStrSize];
    settime(curTime);


	strcpy(toSend, "\nRemote Process Manager V0.01. The time and date on the server is ");
	strcat(toSend, curTime);
	strcat(toSend, ".\n\n\n>");

	se = send(conn_id, toSend, sizeof(toSend), 0);
	if(se == -1){
		error("Error sending time!\n");
	}

	while(1){
		strcpy(command, "");
		n=0;

		//read from connection into the buffer
		n = recv(conn_id, command, sizeof(command), 0);

		//checks if receiving data was successful
		if(n == -1){
			error("Error receiving command!\n");
		}

        // separate with strtok
        char arg[10][256]; //max 10 arguments
        int i = 0;
        for(i=0;i<10;i++){
            arg[i][0] = '\0';
        }
        char *tok;
        i = 0;
        tok = strtok(command, " ");
        while(tok != NULL && i < 10){
            strcpy(arg[i], tok);
            i++;
            tok = strtok(NULL, " ");
        }
        arg[i][0] = '\0';

        // make command string (arg[0]) to lowercase
        toLower(arg[0], n); //n is size in bytes

		//commands
		if (strcmp(arg[0], "exit") == 0){
            break;
		}
		else if (strcmp(arg[0], "pl") == 0){
		    printf("Pl command sent by client ip %s in process %d.\n", clientIp, getpid());
            strcpy(toSend, "top -n 1");
            sys(toSend);

            se = send(conn_id, toSend, sizeof(toSend), 0);
            if(se == -1){
                error("Error sending!\n");
            }
		}
		else if (strcmp(arg[0], "run") == 0){
		    printf("Run command sent by client ip %s in process %d.\n", clientIp, getpid());
            pid_t rpid = fork();
            if(rpid < 0){
                error("The fork was unsuccessful!\n");
            }
            if(rpid == 0){
                if(arg[1][0] == '/'){
                    strcpy(command, arg[1]);
                }
                else{
                    strcpy(command, "/bin/");
                    strcat(command, arg[1]);
                }
                printf("\n");
                if (arg[2][0] == '\0'){
                    execl(command, command, (char *) 0);
                }
                else{
                    execl(command, command, arg[2], arg[3], arg[4], arg[5], arg[6], arg[7], arg[8], arg[9], (char *) 0);
                }
                exit(0);
            }
            char spid[25];
            sprintf(spid, "%d", rpid); //convert rpid to string
            strcpy(toSend, "Going to be run with process id ");
            strcat(toSend, spid);
            strcat(toSend, "\n\n>");

            se = send(conn_id, toSend, sizeof(toSend), 0);
            if(se == -1){
                error("Error sending!\n");
            }
            sleep(1);
            printf("\n");

		}
		else if(strcmp(arg[0], "watch") == 0){
            printf("Watch command sent by client ip %s in process %d.\n", clientIp, getpid());
            for(i=2; i<10; i++){
                w[i-2].pid = atoi(arg[i]);
                w[i-2].time = atoi(arg[1]);
                w[i-2].conn_id = conn_id;
            }
            //void *threadNum[8];
            int threadNum[8];

            pthread_t watchThread[8];
            int iret[8];

            pthread_mutex_init(&mutex, NULL);

            for(i=0; w[i].pid > 0; i++){
                threadNum[i] = i;
                iret[i] = pthread_create(&watchThread[i], NULL, watchFunction, &threadNum[i]);
                if(iret[i] != 0){
                    error("Thread not generated!\n");
                }

            }

            int watchLen = atoi(arg[1]);//w[0].time;
            time_t endTime = time(NULL) + watchLen;
            time_t newEndTime = time(NULL) + 5;
            while(1){
                if(time(NULL) >= newEndTime){
                    pthread_mutex_lock(&mutex);
                    strcat(watchLog, "\n");
                    se = send(conn_id, watchLog, sizeof(watchLog), 0);
                    if(se == -1){
                        error("Error sending!\n");
                    }
                    strcpy(watchLog, "");
                    pthread_mutex_unlock(&mutex);
                    newEndTime = time(NULL) + 5;
                }
                if (time(NULL) >= endTime){
                    pthread_mutex_lock(&mutex);
                    if(watchLog[0] != '\0'){
                        strcat(watchLog, "\n");
                        se = send(conn_id, watchLog, sizeof(watchLog), 0);
                        if(se == -1){
                            error("Error sending!\n");
                        }
                        strcpy(watchLog, "");
                    }
                    pthread_mutex_unlock(&mutex);

                    break;
                }

            }

            for(i=0; w[i].pid > 0; i++){
                pthread_join(watchThread[i], NULL);
            }

            strcpy(toSend, "Watch finished successfully.\n\n>");
            se = send(conn_id, toSend, sizeof(toSend), 0);
            if(se == -1){
                error("Error sending!\n");
            }
		}
		else{
		    printf("Unknown command sent by client ip %s in process %d.\n", clientIp, getpid());
		    strcpy(toSend, "Unknown command!\n\n>");
            se = send(conn_id, toSend, sizeof(toSend), 0);
            if(se == -1){
                error("Error sending!\n");
            }
		}


	}

	//Closes the connection
    close(conn_id);
    printf("Connection closed with client ip %s in process %d.\n", clientIp, getpid());
}

void sys(char cmd[]){
    char fileName[256];
    strcpy(fileName,"tempFile");
    int pid = getpid();
    char spid[25];
    sprintf(spid, "%d", pid); //convert pid to string
    strcat(fileName, spid); //appends pid to filename to make unique to this process

    strcat(cmd, " > ");
    strcat(cmd, fileName);

    system(cmd); //call system and output to file

    //open file and copy content to cmd
    FILE *in;
    in = fopen(fileName, "r");

    strcpy(cmd, "");
    int i = 0;
    char ch;
    while((ch=fgetc(in)) != EOF){
        cmd[i] = ch;
        i++;
    }
    fclose(in);
    cmd[i] = '\0';
    strcat(cmd, "\n>");

    //delete file
    char newCmd[256];
    strcpy(newCmd, "rm ");
    strcat(newCmd, fileName);
    system(newCmd);
}

void settime(char curTime[])
{
	time_t rawtime;
    struct tm * timeinfo;

    time (&rawtime);
    timeinfo = localtime (&rawtime);
    //printf ("Current local time and date: %s\n", asctime(timeinfo));
    strcpy(curTime,asctime(timeinfo));
    curTime[strlen(curTime)-1] = '\0';
}

void toLower(char str[], int n){
    n = n / (sizeof(char)); //the size of a char is 1. i'm doing this in case i reuse this code for a different type.
    char temp[n];
    int i;
    for(i=0; i<n; i++){
        temp[i] = tolower(str[i]);
        str[i] = temp[i];
    }
}

void error(const char *msg)
{
    perror(msg);
    exit(1);
}

long get_process_mem(int prc_id)
{
	char proc_addr[100];
	sprintf(proc_addr,"/proc/%d/statm", prc_id);

	long mem_usage;
	FILE * pfp = fopen(proc_addr, "r");
	fscanf(pfp, "%ld %*ld %*ld %*ld %*ld %*ld %*ld", &mem_usage);
	fclose(pfp);
	return mem_usage;
}

void *watchFunction(void *threadNum){
    int num =* (int *) threadNum;
    //printf("%d %d %d\n", w[num].time, w[num].pid, w[num].conn_id);

    int prId = w[num].pid;
    int conn_id = w[num].conn_id;
    long mem;
    char watchStr[1024], watchTemp[16], prIdStr[16];
    int watchLen = w[num].time;
    time_t endTime = time(NULL) + watchLen;
    while (time(NULL) < endTime){
        mem = get_process_mem(prId);
        strcpy(watchStr, "Memory usage for process ");
        sprintf(prIdStr, "%d", prId);
        strcat(watchStr, prIdStr);
        strcat(watchStr, " is ");
        sprintf(watchTemp, "%ld", mem);
        strcat(watchStr, watchTemp);
        strcat(watchStr, " pages.\n");
        pthread_mutex_lock(&mutex);
        strcat(watchLog, watchStr);
        pthread_mutex_unlock(&mutex);

        sleep(1);
    }
}

void *interface(void){
    char in = '\0';
    printf("\nRemote process manager (server) running...\n\t(Enter q to exit)\n\n");

    while(1){
        scanf("%c", &in);
        if(in == 'q'){
            printf("Do you want to close open connections? (y/n)\n");
            printf("\ty: This will kill all processes. Connected clients may crash.\n");
            printf("\tn: This will close the server. Connected clients will stay connected with limited functionality.\n\n");
            do{
                scanf(" %c", &in);
                if(!(in == 'n' || in == 'N' || in == 'y' || in == 'Y')){
                    printf("Invalid entry! (y/n)\n\n");
                }
            } while (!(in == 'n' || in == 'N' || in == 'y' || in == 'Y'));

            if(in == 'n' || in == 'N'){
                exit(0);
            }
            else if (in == 'y' || in == 'Y'){
                int i;
                for(i=0; i<pidsNum;i++){
                    kill(pids[i], SIGKILL);
                }
                exit(0);
            }
        }
    }
}
